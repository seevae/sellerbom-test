package com.tomjshore.dependency.resolver;

import com.tomjshore.dependency.Dependency;

import java.util.Set;
import java.util.concurrent.ExecutorService;

public interface BuildableDependencyResolver<T extends Resolver<Set<Dependency>>> {

  /**
   * Builds a new instance of {@link Resolver}
   * 
   * @param executor to do used for threading
   * @return instance of {@link Resolver}
   */
  T build(ExecutorService executor);
}
