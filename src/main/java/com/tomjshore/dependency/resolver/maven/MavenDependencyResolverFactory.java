package com.tomjshore.dependency.resolver.maven;

import com.tomjshore.dependency.resolver.BuildableDependencyResolver;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.ExecutorService;

public class MavenDependencyResolverFactory
    implements BuildableDependencyResolver<MavenDependencyResolver> {
  @Override
  public MavenDependencyResolver build(ExecutorService executor) {
    try {
      return new MavenDependencyResolver(executor,
          new MavenRepositoryApi(new URI("https://repo1.maven.org/maven2")));
    } catch (URISyntaxException e) {
      return null;
    }
  }
}
