package com.tomjshore.dependency.resolver.maven;

public record MavenCheckSum(String sha1, String md5) {
}
