package com.tomjshore.dependency;

import com.tomjshore.ChecksumType;

import java.util.*;

/**
 * Model of a Software Dependency
 */
public class Dependency {


  private Purl purl;
  private final Set<Dependency> children;
  private final Map<ChecksumType, String> checksums;

  public Dependency() {
    this.children = new HashSet<>();
    this.checksums = new HashMap<>(5);
  }

  public Dependency(Purl purl) {
    this();
    this.setPurl(purl);
  }

  public void setPurl(Purl purl) {
    this.purl = purl;
  }

  /**
   * Get the current purl can be null
   * 
   * @return a purl
   */
  public Purl getPurl() {
    return purl;
  }

  /**
   * All the children dependencies
   * 
   * @return Set of the next level of dependencies
   */
  public Set<Dependency> getChildren() {
    return this.children;
  }

  /**
   * Adds a new child dependency to the children
   * 
   * @param child to add
   */
  public void addChild(Dependency child) {
    this.children.add(child);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o)
      return true;
    if (o == null || getClass() != o.getClass())
      return false;
    Dependency that = (Dependency) o;
    return Objects.equals(purl, that.purl);
  }

  @Override
  public int hashCode() {
    return Objects.hash(purl);
  }

  @Override
  public String toString() {
    return this.purl.toString();
  }

  public String getChecksum(ChecksumType checksumType) {
    return this.checksums.get(checksumType);
  }

  public void setChecksum(ChecksumType checksumType, String checksum) {
    this.checksums.put(checksumType, checksum);
  }
}
