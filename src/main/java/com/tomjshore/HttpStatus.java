package com.tomjshore;

public class HttpStatus {
  public static int OK = 200;
  public static int NOT_FOUND = 404;
}
