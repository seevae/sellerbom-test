package com.tomjshore.xml;

import org.dom4j.io.SAXReader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * <p>
 * Use for xml operations.
 * </p>
 * <p>
 * Objects returned from this utility class should be safe because they follow <a href=
 * "https://cheatsheetseries.owasp.org/cheatsheets/XML_External_Entity_Prevention_Cheat_Sheet.html">
 * owasp cheatsheet</a>
 * </p>
 */
public class SafeXml {

  private static final Logger LOG = LoggerFactory.getLogger(SafeXml.class);

  /**
   * A {@link org.dom4j.io.SAXReader} with the following features
   * <ul>
   * <li>http://apache.org/xml/features/disallow-doctype-decl = true</li>
   * <li>http://xml.org/sax/features/external-general-entities = false</li>
   * <li>http://xml.org/sax/features/external-parameter-entities false</li>
   * </ul>
   * 
   * @return a safe SAXReader
   */
  public static SAXReader saxReader() {
    SAXReader reader = SAXReader.createDefault();
    try {
      reader.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
    } catch (SAXException e) {
      LOG.error("Could not set http://apache.org/xml/features/disallow-doctype-decl feature", e);
      System.exit(1);
    }
    return reader;
  }
}
