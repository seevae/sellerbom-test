package com.tomjshore.file;

import com.tomjshore.DependencyManagementTool;

import java.io.File;

/**
 * A file of interest to the dependency management tool, for example pom.xml is of interest to maven.
 */
public record DependencyFile(File file, DependencyManagementTool dependencyManagementTool) {
}
