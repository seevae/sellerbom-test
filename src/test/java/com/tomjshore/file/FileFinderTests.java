package com.tomjshore.file;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import com.tomjshore.DependencyManagementTool;
import java.util.Collection;
import java.util.Map;
import java.util.regex.Pattern;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FileFinderTests {

  private FileFinder fileFinder;

  @BeforeEach
  void setUp() {
    Map<Pattern, DependencyManagementTool> patterns =
        Map.of(Pattern.compile("pom.xml"), DependencyManagementTool.MAVEN);
    fileFinder = new FileFinder(patterns);
  }

  @Test
  void testFindReturnsEmptyCollectionWhenRootDoesNotExist() {
    // given
    String addressToFile = "/i/dont/exist";

    // when
    Collection<DependencyFile> stuff = fileFinder.find(addressToFile);

    // then
    assertTrue(stuff.isEmpty());
  }

  @Test
  void testFindReturnsEmptyCollectionWhenRootIsAFileNotDir() {
    // given
    String addressToRoot = this.getClass().getResource("/files/folder-with-pom/pom.xml").getPath();

    // when
    Collection<DependencyFile> stuff = fileFinder.find(addressToRoot);

    // then
    assertTrue(stuff.isEmpty());
  }

  @Test
  void testFindReturnsPomWhenRootContainsPom() {
    // given
    String addressToRoot = this.getClass().getResource("/files/folder-with-pom").getPath();

    // when
    Collection<DependencyFile> paths = fileFinder.find(addressToRoot);

    // then
    assertEquals(1, paths.size());
  }

  @Test
  void testFindWithCorrectDataWhenRootContainsPom() {
    // given
    String addressToRoot = this.getClass().getResource("/files/folder-with-pom").getPath();

    // when
    Collection<DependencyFile> paths = fileFinder.find(addressToRoot);

    // then
    DependencyFile dependencyFile = paths.stream().findAny().get();
    assertEquals(addressToRoot + "/pom.xml", dependencyFile.file().getPath());
    assertEquals(DependencyManagementTool.MAVEN, dependencyFile.dependencyManagementTool());
  }

  @Test
  void testFindsPomWhenPomIsInSubFolder() {
    // given
    String addressToRoot = this.getClass().getResource("/files/folder-with-sub-folder").getPath();

    // when
    Collection<DependencyFile> paths = fileFinder.find(addressToRoot);

    // then
    assertEquals(1, paths.size());
    DependencyFile dependencyFile = paths.stream().findAny().get();
    assertEquals(addressToRoot + "/sub-folder/pom.xml", dependencyFile.file().getPath());
    assertEquals(DependencyManagementTool.MAVEN, dependencyFile.dependencyManagementTool());
  }
}
