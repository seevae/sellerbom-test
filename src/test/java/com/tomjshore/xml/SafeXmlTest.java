package com.tomjshore.xml;

import org.dom4j.io.SAXReader;
import org.junit.jupiter.api.Test;
import org.xml.sax.SAXException;

import static org.junit.jupiter.api.Assertions.*;

class SafeXmlTest {

  @Test
  void testSAXReaderReturnsNewInstanceOfSAXReader() {
    // when
    SAXReader reader = SafeXml.saxReader();

    // then
    assertNotNull(reader);
  }

  @Test
  void testSAXReaderReturnsSAXReaderWithCorrectFeaturesTurnedOn() throws SAXException {
    // when
    SAXReader reader = SafeXml.saxReader();

    // then
    assertTrue(
        reader.getXMLReader().getFeature("http://apache.org/xml/features/disallow-doctype-decl"));
    assertFalse(reader.getXMLReader()
        .getFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd"));
    assertFalse(
        reader.getXMLReader().getFeature("http://xml.org/sax/features/external-general-entities"));
    assertFalse(reader.getXMLReader()
        .getFeature("http://xml.org/sax/features/external-parameter-entities"));
  }

}
