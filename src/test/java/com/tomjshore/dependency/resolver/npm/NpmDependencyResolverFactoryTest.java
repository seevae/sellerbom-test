package com.tomjshore.dependency.resolver.npm;

import org.junit.jupiter.api.Test;

import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertNotNull;

class NpmDependencyResolverFactoryTest {

  @Test
  void testBuildReturnsMavenDependencyResolver() {
    // given
    NpmDependencyResolverFactory factory = new NpmDependencyResolverFactory();
    // when
    NpmDependencyResolver resolver = factory.build(Executors.newSingleThreadExecutor());
    // then
    assertNotNull(resolver);
  }
}
