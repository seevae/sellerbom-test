package com.tomjshore.dependency;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PurlTest {

  @Test
  void testToStringPrintsInCorrectFormat() {
    // given
    String expectedFormat = "pkg:maven/org.springframework/spring-core@5.3.9";

    // when
    Purl purl = new Purl(PurlType.MAVEN, "org.springframework", "spring-core", "5.3.9");

    // then
    assertEquals(expectedFormat, purl.toString());
  }

  @Test
  void testToStringPrintsInCorrectFormatWithNoNamespace() {
    // given
    String expectedFormat = "pkg:nuget/EnterpriseLibrary.Common@6.0.1304";

    // when
    Purl purl = new Purl(PurlType.NUGET, null, "EnterpriseLibrary.Common", "6.0.1304");

    // then
    assertEquals(expectedFormat, purl.toString());
  }

  @Test
  void testToStringPrintsInCorrectFormatWithNoVersion() {
    // given
    String expectedFormat = "pkg:github/package-url/purl-spec";

    // when
    Purl purl = new Purl(PurlType.GITHUB, "package-url", "purl-spec", null);

    // then
    assertEquals(expectedFormat, purl.toString());
  }

  @Test
  void testToStringPrintsInCorrectFormatWithNoNamespaceOrVersion() {
    // given
    String expectedFormat = "pkg:pypi/requests";

    // when
    Purl purl = new Purl(PurlType.PYPI, null, "requests", null);

    // then
    assertEquals(expectedFormat, purl.toString());
  }

}
