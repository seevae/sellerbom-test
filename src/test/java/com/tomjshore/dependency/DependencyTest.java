package com.tomjshore.dependency;

import org.junit.jupiter.api.Test;

import java.util.Set;

import static com.tomjshore.ChecksumType.MD5;
import static org.junit.jupiter.api.Assertions.*;

class DependencyTest {


  @Test
  void testGetterSetterOfPurl() {
    // given
    Purl expectedPurl = new Purl(PurlType.CONAN, "google", "gson", "1.64.2");
    Dependency dependency = new Dependency();

    // when
    dependency.setPurl(expectedPurl);

    // then
    assertEquals(expectedPurl, dependency.getPurl());
  }

  @Test
  void testConstructorWithPurl() {
    // given
    Purl expectedPurl = new Purl(PurlType.NUGET, null, "EnterpriseLibrary.Common", "6.0.1304");

    // when
    Dependency dependency = new Dependency(expectedPurl);

    // then
    assertEquals(expectedPurl, dependency.getPurl());
  }

  @Test
  void testGetChildrenReturnsNoKids() {
    // given
    Dependency dependency = new Dependency();

    // when
    Set<Dependency> kids = dependency.getChildren();

    // then
    assertNotNull(kids);
    assertTrue(kids.isEmpty());
  }

  @Test
  void testAddChildReturnsOneKid() {
    // given
    Dependency root =
        new Dependency(new Purl(PurlType.MAVEN, "org.junit.jupiter", "junit-jupiter-api", "5.8.0"));
    Dependency child =
        new Dependency(new Purl(PurlType.MAVEN, "org.opentest4j", "opentest4j", "1.2.0"));

    root.addChild(child);

    // when
    Set<Dependency> kids = root.getChildren();

    // then
    assertEquals(1, kids.size());
    assertEquals(child, kids.stream().findAny().get());
  }

  @Test
  void testGetCheckSumReturnsValue() {
    // given
    Dependency dependency = new Dependency(new Purl(PurlType.NPM, null, "react", "17.0.2"));
    String expectedMd5 = "d61896ed1295af914c6664b429347e7e";
    dependency.setChecksum(MD5, expectedMd5);

    // when
    String result = dependency.getChecksum(MD5);

    // then
    assertEquals(expectedMd5, result);
  }

  @Test
  void testGetCheckSumReturnsNullWhenNotSet() {
    // given
    Dependency dependency = new Dependency(new Purl(PurlType.NPM, null, "angular", "1.8.2"));

    // when
    String result = dependency.getChecksum(MD5);

    // then
    assertNull(result);
  }

  @Test
  void testEqualsJustPurl() {
    // given
    Dependency dependency1 = new Dependency(new Purl(PurlType.PYPI, null, "Flask", "2.0.1"));
    Dependency dependency2 = new Dependency(new Purl(PurlType.PYPI, null, "Flask", "2.0.1"));

    // when
    boolean result = dependency1.equals(dependency2);

    // then
    assertTrue(result);
  }

  @Test
  void testEqualsWithChildren() {
    // given
    Dependency dependency1 = new Dependency(new Purl(PurlType.PYPI, null, "Flask", "2.0.1"));
    Dependency subDependency1 = new Dependency(new Purl(PurlType.PYPI, null, "Jinja2", "3.0"));
    dependency1.addChild(subDependency1);

    Dependency dependency2 = new Dependency(new Purl(PurlType.PYPI, null, "Flask", "2.0.1"));
    Dependency subDependency2 = new Dependency(new Purl(PurlType.PYPI, null, "Jinja2", "3.0"));
    dependency2.addChild(subDependency2);

    // when
    boolean result = dependency1.equals(dependency2);

    // then
    assertTrue(result);
  }
}
