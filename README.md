# SellerBOM or: How I Learned to Stop Worrying and Love the SBOM

This is a command line tool that take source code from a supported language and dependency management tool and tries to generate an SBOM from it.
What is an [SBOM](https://www.linuxfoundation.org/blog/what-is-an-sbom/) I hear you ask?  Well it's a list of dependencies, lib and tools used to build and run a piece of software. 

## Build
You will need an JDK 17 and Maven. 
```mvn package```

## Run
TODO

## Contribute 
Anyone is free to contribute, I only ask if you are doing a code change please follow Test Driven Development
### Style guide 
We are using the [Google Style guide](https://google.github.io/styleguide/javaguide.html) the best way to make sure you are using this style is by running ```mvn spotless:apply``` before a commit 